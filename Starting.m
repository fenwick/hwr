%% Conveting to binary image
I=imread('inverted.jpg');
figure;
imshow(I);
figure;
%bw=im2bw(I,0.4);
bw=rgb2gray(I);
imshow(bw);

%% Resizing the array
ID=imresize(bw,[200 200]);
figure;
imshow(ID);

%% Breaking array into cells

t=zeros(100,400);
for x=1:10
    for y=1:10
        xd=(x-1)*20;
        yd=(y-1)*20;
        j=1;
        for yt=1:20
            for xt=1:20
                if ID(xd+xt,yd+yt)>50
                    t((x-1)*10+y,(yt-1)*20+xt)=(ID(xd+xt,yd+yt));
                end
            end
        end
    end
end

for x=1:100
    for y=1:400
        t(x,y)=t(x,y)/257;
    end
end
displayData(t);

